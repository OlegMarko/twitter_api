<?php

/* app.html */
class __TwigTemplate_22a518250af2604f7695b53a13803aad5ca8689bb178679a1863c6c327a127e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>";
        // line 8
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "</title>
</head>
<body>

<form action=\"/\">
    <input type=\"search\" required name=\"search_query\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, ($context["search_query"] ?? null), "html", null, true);
        echo "\" placeholder=\"enter your query\">
    <input type=\"submit\" value=\"Search\">
</form>

<br>
<hr>
<br>

<h1>search results</h1>

";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 24
            echo "
<p>
    ";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "text", array()), "html", null, true);
            echo "<br>
    Posted on:
    <a href=\"https://twitter.com/";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "user", array()), "screen_name", array()), "html", null, true);
            echo "/status/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "id_str", array()), "html", null, true);
            echo "\" target=\"_blank\">
        ";
            // line 29
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "created_at", array()), "Y-m-d H:i"), "html", null, true);
            echo "
    </a>
</p>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "</body>
</html>";
    }

    public function getTemplateName()
    {
        return "app.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 34,  68 => 29,  62 => 28,  57 => 26,  53 => 24,  49 => 23,  36 => 13,  28 => 8,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "app.html", "C:\\xampp\\htdocs\\test\\public\\layouts\\app.html");
    }
}
