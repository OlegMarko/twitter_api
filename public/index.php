<?php

error_reporting(-1);

$query = rtrim($_SERVER['QUERY_STRING'], '/');

define('WWW', __DIR__);
define('ROOT', dirname(__DIR__));

require_once ROOT . '/vendor/autoload.php';
require_once ROOT . '/config/app.php';

$loader = new Twig_Loader_Filesystem(
    WWW . '/layouts'
);
$twig = new Twig_Environment(
    $loader
);

$template = $twig->load('app.html');


/**
 * Return response by twitter search api
 *
 * @param $search
 * @param $config
 * @return string
 */
function queryTwitter($search, $config)
{
    $api_twitter_url = "https://api.twitter.com/1.1/";
    $api_search_url = "search/tweets.json";
    $url = $api_twitter_url . $api_search_url;
    if ($search != "") {
        $search = "#" . $search;
    }
    $query = [
        'count' => 100,
        'q' => urlencode($search),
        "result_type" => "recent"
    ];

    $config['oauth_access_token'];

    $oauth_access_token = $config['oauth_access_token'];
    $oauth_access_token_secret = $config['oauth_access_token_secret'];
    $consumer_key = $config['consumer_key'];
    $consumer_secret = $config['consumer_secret'];

    $oauth = [
        'oauth_consumer_key' => $consumer_key,
        'oauth_nonce' => time(),
        'oauth_signature_method' => 'HMAC-SHA1',
        'oauth_token' => $oauth_access_token,
        'oauth_timestamp' => time(),
        'oauth_version' => '1.0'
    ];

    $base_params = empty($query) ? $oauth : array_merge($query, $oauth);
    $base_info = buildBaseString($url, 'GET', $base_params);

    $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
    $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
    $oauth['oauth_signature'] = $oauth_signature;

    $header = array(buildAuthorizationHeader($oauth), 'Expect:');

    $pest = new Pest($api_twitter_url);

    $searches = $pest->get(
        $api_search_url,
        $query,
        $header
    );

    return $searches;
}

/**
 * Get search query
 *
 * @param $baseURI
 * @param $method
 * @param $params
 * @return string
 */
function buildBaseString($baseURI, $method, $params)
{
    $r = [];
    ksort($params);
    foreach ($params as $key => $value) {
        $r[] = "$key=" . rawurlencode($value);
    }
    return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
}

/**
 * Get Twitter authorization string
 *
 * @param $oauth
 * @return string
 */
function buildAuthorizationHeader($oauth)
{
    $r = 'Authorization: OAuth ';
    $values = [];
    foreach ($oauth as $key => $value) {
        $values[] = "$key=\"" . rawurlencode($value) . "\"";
    }
    $r .= implode(', ', $values);
    return $r;
}

$searches = "{}";
$search_query = '';

if (isset($_GET['search_query'])) {
    $search_query = $_GET['search_query'];

    try {
        $searches = queryTwitter($search_query, $config['twitter']);
    } catch (Pest_Exception $e) {
    }
}


$data = [];
$data = json_decode($searches, true);
$data = isset($data['statuses']) ? $data['statuses'] : [];
$title = 'Search using twitter api';

echo $template->render([
    'title' => $title,
    'data' => $data,
    'search_query' => $search_query
]);

die();
